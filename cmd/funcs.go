/*
 * funcs.go - General purpose functions for executing either standalone, or as part of another project.
 *     Copyright (C) 2023  Athar Shaikh
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cmd

import (
	"gitlab.com/atharshaikh/pkgs-server/db"
	"gitlab.com/atharshaikh/pkgs-server/distributions"
	"log"
)

func CleanAndUpdateDB() {
	db1 := db.GetDB()

	rowsDeleted, err := db.ClearDB(db1)
	if err != nil {
		log.Printf("Error while clearing database: %s", err)
	}
	log.Printf("Deleted %d rows", rowsDeleted)

	err = db.RunMigrations(db1)
	if err != nil {
		log.Fatalf("Unable to run migrations: %s", err)
	}
	log.Println("Migrations Completed")

	dists := []distributions.Distribution{&distributions.Debian{}}
	for _, i := range dists {
		i.GetPackages(db1)
	}
}
