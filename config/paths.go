/*
 * config.go - config variables and types
 *     Copyright (C) 2023  Athar Shaikh
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"github.com/adrg/xdg"
	"log"
	"os"
	"path/filepath"
)

var DataHome = filepath.Join(xdg.DataHome, "pkgs")
var PathToDb = filepath.Join(DataHome, "data.db")

func CheckDataHome() {
	if _, err := os.Stat(DataHome); os.IsNotExist(err) {
		err := os.Mkdir(DataHome, 0700)
		if err != nil {
			log.Fatalf("could not create directory %s, try manually creating it", DataHome)
		}
	}
}
