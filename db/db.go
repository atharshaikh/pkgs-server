/*
 * db.go - models and functions related to db
 *     Copyright (C) 2023  Athar Shaikh
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package db

import (
	"gitlab.com/atharshaikh/pkgs-server/config"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"log"
)

type Packages struct {
	Distribution string
	DistVersion  string `gorm:"primaryKey"`
	Repo         string `gorm:"primaryKey"`
	Package      string `gorm:"primaryKey"`
	Version      string `gorm:"primaryKey"`
	Section      string
	Priority     string
	Architecture string
	Description  string
}

func GetDB() *gorm.DB {
	config.CheckDataHome()
	db, err := gorm.Open(sqlite.Open(config.PathToDb), &gorm.Config{CreateBatchSize: 250})
	if err != nil {
		log.Fatalf("Could not create GORM connection to database: %s", err)
	}
	log.Printf("Connected to database at %s\n", config.PathToDb)
	return db
}

func RunMigrations(db *gorm.DB) error {
	err := db.AutoMigrate(&Packages{})
	return err
}

func ClearDB(db *gorm.DB) (int64, error) {
	result := db.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(Packages{})
	return result.RowsAffected, result.Error
}
