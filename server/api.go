/*
 * api.go - run gRPC and HTTP server
 *     Copyright (C) 2023  Athar Shaikh
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package server

import (
	"context"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	db2 "gitlab.com/atharshaikh/pkgs-server/db"
	"gitlab.com/atharshaikh/pkgs-server/pkgs_proto_go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
	"log"
	"net"
	"net/http"
	"time"
)

var gRPCServer = ":9000"
var httpServer = "8090"

type Server struct {
	pkgs_proto_go.UnsafeDBPackageQueryServiceServer
	db *gorm.DB
}

func unaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	start := time.Now()
	h, err := handler(ctx, req)

	log.Printf("Method Called:%s\tTime Taken:%s\t\n",
		info.FullMethod,
		time.Since(start),
	)

	return h, err
}

func (s *Server) FetchPackageDetails(ctx context.Context, in *pkgs_proto_go.FetchPackageDetailsRequest) (*pkgs_proto_go.FetchPackageDetailsResponse, error) {
	var pkg db2.Packages
	db := s.db

	result := db.Where(&db2.Packages{Package: in.GetQuery(), DistVersion: in.GetDist()}).First(&pkg)

	if result.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "Could not find a package with that name")
	}

	return &pkgs_proto_go.FetchPackageDetailsResponse{
		Name:        pkg.Package,
		Version:     pkg.Version,
		Description: pkg.Description,
		Repo:        pkg.Repo,
	}, nil
}

func (s *Server) SearchForPackage(ctx context.Context, request *pkgs_proto_go.SearchForPackageRequest) (*pkgs_proto_go.SearchForPackageResponse, error) {
	var pkg []db2.Packages
	var pkgList []*pkgs_proto_go.FetchPackageDetailsResponse
	db := s.db

	quer := fmt.Sprintf("%%%s%%", request.GetQuery())
	result := db.Where("package LIKE ?", quer).Find(&pkg)
	if result.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "Could not find any packages with that name")
	}

	for _, item := range pkg {
		pkgList = append(pkgList, &pkgs_proto_go.FetchPackageDetailsResponse{
			Name:        item.Package,
			Version:     item.Version,
			Description: item.Description,
			Repo:        item.Repo,
		})
	}
	return &pkgs_proto_go.SearchForPackageResponse{Details: pkgList}, nil
}

func RunServer() {
	lis, err := net.Listen("tcp", gRPCServer)
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	s := grpc.NewServer(grpc.UnaryInterceptor(unaryInterceptor))
	pkgs_proto_go.RegisterDBPackageQueryServiceServer(s, &Server{db: db2.GetDB()})

	log.Printf("Serving gRPC on %s", lis.Addr().String())

	go func() {
		log.Fatalln(s.Serve(lis))
	}()

	conn, err := grpc.DialContext(
		context.Background(),
		lis.Addr().String(),
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}

	gatewayMux := runtime.NewServeMux()
	err = pkgs_proto_go.RegisterDBPackageQueryServiceHandler(context.Background(), gatewayMux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}

	gatewayServer := &http.Server{
		Addr:    fmt.Sprintf(":%s", httpServer),
		Handler: gatewayMux,
	}

	log.Printf("Serving gRPC-Gateway on %s\n", gatewayServer.Addr)
	log.Fatalln(gatewayServer.ListenAndServe())
}
