/*
 * debian.go - fetch packages from debian/debian-like repositories
 *     Copyright (C) 2023  Athar Shaikh
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package distributions

import (
	"compress/gzip"
	"fmt"
	"gitlab.com/atharshaikh/pkgs-server/db"
	"gorm.io/gorm"
	"io"
	"log"
	"net/http"
	"reflect"
	"regexp"
	"strings"
)

type DebUrl struct {
	url  string
	dist string
	repo string
}

type Debian struct {
	baseUrl      string
	urls         []DebUrl
	distVersions []string
	repos        []string
}

func (d *Debian) setDefaultParams() {
	d.baseUrl = "https://ftp.debian.org/debian/dists/%s/%s/binary-all/Packages.gz"
	d.distVersions = []string{"stable", "testing", "unstable", "oldstable"}
	d.repos = []string{"main", "contrib", "non-free"}
}

func (d *Debian) addUrls(urls []DebUrl) {
	d.urls = append(d.urls, urls...)
}

func (d *Debian) setDefaultUrls() {
	var urls []DebUrl
	for _, dist := range d.distVersions {
		for _, repo := range d.repos {
			url := fmt.Sprintf(d.baseUrl, dist, repo)
			urls = append(urls, DebUrl{
				url:  url,
				dist: dist,
				repo: repo,
			})
		}
	}
	d.addUrls(urls)
}

func DebPkgListFromURL(dUrl DebUrl) ([]db.Packages, error) {
	log.Printf("Fetching %s - %s", dUrl.dist, dUrl.repo)

	urlResp, err := fetchURL(dUrl.url)
	if err != nil {
		log.Printf("Error while trying to get: %s\t%s", dUrl.url, err)
		return nil, err
	}

	gzContents, err := extractGZ(urlResp)
	if err != nil {
		log.Printf("Error while extracting of %s - %s", dUrl.dist, dUrl.repo)
		return nil, err
	}

	p := parsePackages(gzContents, dUrl)
	log.Printf("Fetched %d packages", len(p))
	return p, nil
}

func fetchURL(url string) (*http.Response, error) {
	data, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func extractGZ(data *http.Response) ([]byte, error) {
	fz, err := gzip.NewReader(data.Body)
	if err != nil {
		return nil, err
	}

	dataRead, err := io.ReadAll(fz)
	if err != nil {
		return nil, err
	}

	err = fz.Close()
	if err != nil {
		return nil, err
	}

	return dataRead, nil
}

func parsePackages(body []byte, d DebUrl) []db.Packages {
	data := string(body)
	indBody := strings.Split(data, "\n\n")
	dataFields := []string{"Package", "Version", "Section", "Priority", "Architecture", "Description"}
	var packages []db.Packages
	for _, val := range indBody {
		pkg := db.Packages{Distribution: "debian", DistVersion: d.dist, Repo: d.repo}
		for _, field := range dataFields {
			r, _ := regexp.Compile(fmt.Sprintf("%s:\\s*(.*)", field))
			x := r.FindStringSubmatch(val)
			if len(x) != 2 {
				break
			}
			reflect.ValueOf(&pkg).Elem().FieldByName(field).SetString(x[1])
		}
		packages = append(packages, pkg)
	}
	return packages
}

func (d *Debian) GetPackages(db *gorm.DB) {
	d.setDefaultParams()
	d.setDefaultUrls()
	for _, dUrl := range d.urls {
		p, err := DebPkgListFromURL(dUrl)
		if err != nil {
			log.Println(err)
		}
		result := db.Create(&p)
		log.Printf("Inserted %d packages for %s %s", result.RowsAffected, p[0].Distribution, dUrl.dist)
	}
}
